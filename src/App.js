import logo from "./logo.svg";
import "./App.css";
import { Carrinho } from "./components/carrinho";
import { Produtos } from "./components/produtos";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from "react-router-dom";

function App() {
  return (
    <>
      <Router>
        <AppBar position="static">
          <Toolbar>
            <Link to="/">Home</Link>
            <Link to="/cart">carrinho</Link>
          </Toolbar>
        </AppBar>

        <div className="App">
          <header className="App-header">
            <div style={{ display: "flex" }}>
              <Switch>
                <Route path="/cart">
                  <Carrinho />
                </Route>
                <Route path="/">
                  <Produtos />
                </Route>
              </Switch>
            </div>
          </header>
        </div>
      </Router>
    </>
  );
}

export default App;
