const cartReducer = (state = [], action) => {
  switch (action.type) {
    case "ADD_CART":
      const { product } = action;
      const id = product.id;
      return [
        ...state,
        {
          itemId: id,
          item: product,
        },
      ];

    case "REMOVE_CART":
      const { itemId } = action;
      const newList = state.filter((product) => product.itemId !== itemId);
      return newList;

    default:
      return state;
  }
};

export default cartReducer;
