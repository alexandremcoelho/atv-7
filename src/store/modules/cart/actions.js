export const addToCart = (product) => ({ type: "ADD_CART", product });

export const removeFromCart = (itemId) => ({ type: "REMOVE_CART", itemId });
