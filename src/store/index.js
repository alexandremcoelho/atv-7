import { createStore } from "redux";
import { combineReducers } from "redux";

import cartReducer from "./modules/cart/reducer";
import productsReducer from "./modules/products/reducer";

const reducers = combineReducers({
  cart: cartReducer,
  products: productsReducer,
});

const store = createStore(reducers);

export default store;
