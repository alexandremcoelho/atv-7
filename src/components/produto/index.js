import { useDispatch } from "react-redux";
import { addToCart, removeFromCart } from "../../store/modules/cart/actions";
import "../../App.css";

export const Produto = ({ product, isRemovable = false }) => {
  const dispatch = useDispatch();

  return (
    <div className="produto">
      <h1>{product.name}</h1>
      <h2>{product.price}</h2>
      {isRemovable ? (
        <button onClick={() => dispatch(removeFromCart(product.id))}>
          remover
        </button>
      ) : (
        <button onClick={() => dispatch(addToCart(product))}>adicionar</button>
      )}
    </div>
  );
};
