import { Produto } from "../produto";
import { useSelector } from "react-redux";

export const Carrinho = () => {
  const cart = useSelector((state) => state.cart);
  console.log(cart);
  return (
    <div>
      <h1>CARRINHO</h1>
      <div className="produtos">
        {cart.map((produto, index) => (
          <Produto key={index} product={produto.item} isRemovable />
        ))}
      </div>
      <label>Total - </label>
      <label>
        {cart.reduce((acc, product) => acc + product.item.price, 0)}
      </label>
    </div>
  );
};
