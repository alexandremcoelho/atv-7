import { useSelector } from "react-redux";
import { Produto } from "../produto";
import "../../App.css";
export const Produtos = () => {
  const products = useSelector((state) => state.products);
  return (
    <div>
      <h1>loja</h1>
      <div className="produtos">
        {products.map((produto, index) => (
          <Produto key={index} product={produto} />
        ))}
      </div>
    </div>
  );
};
